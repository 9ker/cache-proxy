// +build integration

package main_test

import (
	"bufio"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"syscall"
	"testing"
	"time"

	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/suite"
	"gitlab.com/9ker/cache-proxy/cache"
	"gitlab.com/9ker/cache-proxy/config"
	"gitlab.com/9ker/cache-proxy/route"
	"gitlab.com/9ker/cache-proxy/server"
)

// ITestSuite full suite
type ITestSuite struct {
	suite.Suite
	cfgPath  string
	dataPath string
	proxyUrl string
	rclient  *redis.Client
}

type expectedData struct {
	Data string `json:"data"`
}

func TestIntegration(t *testing.T) {
	testCfgPath := "itgtest-config.yaml"
	dataPath := "itgtest-data.redis"
	suite.Run(t, &ITestSuite{cfgPath: testCfgPath, dataPath: dataPath})
}

func (s *ITestSuite) SetupSuite() {
	// Configure server for integration test
	c, err := config.ParseConfig(s.cfgPath)
	s.Require().NoError(err)
	logrus.SetOutput(ioutil.Discard)
	s.proxyUrl = "http://localhost:" + c.HTTPPort

	s.rclient = redis.NewClient(&redis.Options{
		Addr: c.RedisHost + ":" + c.RedisPort,
	})

	backing := cache.NewBackingCache(c)
	cache := cache.NewCache(c, backing)
	cache.Start()

	proxy := server.NewServer(c, cache)
	proxy.Router.Handle("/", &route.RootHandler{})
	proxy.Router.Handle("/cache", &route.CacheHandler{proxy.Cache})
	// Start the process in beginning of test
	go proxy.Run()
}

func (s *ITestSuite) TearDownSuite() {
	p, _ := os.FindProcess(syscall.Getpid())
	p.Signal(syscall.SIGINT)
}

func (s *ITestSuite) SetupTest() {
	// Consum dataset from file and inject into redis inbetween tests
	f, err := os.Open(s.dataPath)
	s.Require().NoError(err)
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		cmd := strings.Split(scanner.Text(), " ")
		err = s.rclient.Set(cmd[1], cmd[2], time.Hour).Err()
		s.Require().NoError(err)
	}
	s.Require().NoError(scanner.Err())
}

func (s *ITestSuite) TearDownTest() {
	err := s.rclient.FlushAll().Err()
	s.Require().NoError(err)
}

func (s *ITestSuite) TestHealthCheck() {
	fixtures := []struct {
		inputMth     string
		inputData    string
		expectedCode int
		expectedData string
	}{
		{"GET", "", http.StatusOK, `{"version":"0.0.1","name":"Proxy Server for Redis"}`},
		{"POST", "", http.StatusOK, `{"version":"0.0.1","name":"Proxy Server for Redis"}`},
		{"DELETE", "", http.StatusOK, `{"version":"0.0.1","name":"Proxy Server for Redis"}`},
		{"GET", "/no", http.StatusNotFound, "404 page not found"},
		{"GET", "/cache?key=dontexist", http.StatusNotFound, ""},
	}
	for _, fix := range fixtures {
		req, err := http.NewRequest(fix.inputMth, s.proxyUrl+fix.inputData, nil)
		s.NoError(err)

		client := http.Client{}
		response, err := client.Do(req)
		s.NoError(err)
		s.Equal(fix.expectedCode, response.StatusCode)

		byteBody, err := ioutil.ReadAll(response.Body)
		s.NoError(err)

		s.Equal(fix.expectedData, strings.Trim(string(byteBody), "\n"))
		response.Body.Close()
	}
}

func (s *ITestSuite) TestEndToEndWithHit() {
	hook := test.NewGlobal()
	fixtures := []struct {
		inputkey     string
		expectedCode int
		expectedData string
		hitBack      bool
	}{
		{"anchor", http.StatusOK, "OK", true},
		{"YayQrdnnYgama", http.StatusOK, "http://www.google.ca/", true},
		{"testing1", http.StatusOK, "123", true},
		{"testing1", http.StatusOK, "123", false},
	}
	for _, fix := range fixtures {
		req, err := http.NewRequest("GET", s.proxyUrl+"/cache?key="+fix.inputkey, nil)
		s.NoError(err)

		client := http.Client{}
		response, err := client.Do(req)
		s.NoError(err)
		if fix.hitBack {
			s.Require().Equal("HIT backing cache", hook.LastEntry().Message)
		} else {
			s.Require().Equal("HIT local cache", hook.LastEntry().Message)
		}
		s.Equal(fix.expectedCode, response.StatusCode)

		expData := &expectedData{}
		err = json.NewDecoder(response.Body).Decode(expData)
		s.Require().NoError(err)
		s.Equal(fix.expectedData, expData.Data)
		response.Body.Close()
	}
}

func (s *ITestSuite) TestEndToEndSweep() {
	hook := test.NewGlobal()
	fixtures := []struct {
		inputkey     string
		expectedCode int
		expectedData string
		hitBack      bool
	}{
		{"iWillBeRemovedBecauseImOld", http.StatusOK, "Byeyb43", true},
		{"iWillBeRemovedBecauseImOld", http.StatusOK, "Byeyb43", true},
	}
	for _, fix := range fixtures {
		req, err := http.NewRequest("GET", s.proxyUrl+"/cache?key="+fix.inputkey, nil)
		s.NoError(err)

		client := http.Client{}
		response, err := client.Do(req)
		s.NoError(err)
		if fix.hitBack {
			s.Require().Equal("HIT backing cache", hook.LastEntry().Message)
		} else {
			s.Require().Equal("HIT local cache", hook.LastEntry().Message)
		}
		s.Equal(fix.expectedCode, response.StatusCode)

		expData := &expectedData{}
		err = json.NewDecoder(response.Body).Decode(expData)
		s.Require().NoError(err)
		s.Equal(fix.expectedData, expData.Data)
		response.Body.Close()
		// Sleep 3 seconds since ttl is 3 seconds
		time.Sleep(3 * time.Second)
	}
}
