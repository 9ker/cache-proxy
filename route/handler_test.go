// +build !integration

package route_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"gitlab.com/9ker/cache-proxy/cache"
	"gitlab.com/9ker/cache-proxy/route"
)

var (
	alwaysHit cache.Service
	cacheGet  func(key string) (*cache.Data, error)
)

type mockExternalCache struct {
}

func (m *mockExternalCache) Get(key string) (*cache.Data, error) {
	return cacheGet(key)
}
func TestMain(m *testing.M) {
	logrus.SetOutput(ioutil.Discard)
	os.Exit(m.Run())
}

func TestRouter(t *testing.T) {
	// Create nil request and pass to handler
	alwaysHit = &mockExternalCache{}
	cacheGet = func(key string) (*cache.Data, error) {
		return &cache.Data{Key: key, Value: "gotIt"}, nil
	}
	fixtures := []struct {
		expectedCode int
		expectedMsg  string
		inputMth     string
		inputURL     string
	}{
		{http.StatusOK, `{"version":"0.0.1","name":"Proxy Server for Redis"}`, "GET", "/"},
		{http.StatusBadRequest, "", "GET", "/testcache"},
		{http.StatusBadRequest, "", "GET", "/testcache?key"},
		{http.StatusOK, "{\"data\":\"gotIt\"}", "GET", "/testcache?key=key1"},
	}

	r := http.NewServeMux()
	r.Handle("/", &route.RootHandler{})
	r.Handle("/testcache", &route.CacheHandler{Cache: alwaysHit})

	for _, fix := range fixtures {
		rr := httptest.NewRecorder()
		req, err := http.NewRequest(fix.inputMth, fix.inputURL, nil)
		require.Nil(t, err)

		r.ServeHTTP(rr, req)

		require.Equal(t, fix.expectedCode, rr.Code)
		require.Equal(t, fix.expectedMsg, strings.TrimRight(rr.Body.String(), "\n"))
	}
}
