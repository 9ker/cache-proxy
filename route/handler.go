package route

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/9ker/cache-proxy/cache"
)

// RootHandler handles get request
type RootHandler struct{}

// CacheHandler handles cache get
type CacheHandler struct {
	Cache cache.Service
}

// Handles all request on / return generic, can be used as health-check
func (h *RootHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/":
		type Response struct {
			Version string `json:"version"`
			Name    string `json:"name"`
		}
		resp := Response{
			Version: "0.0.1",
			Name:    "Proxy Server for Redis",
		}
		json.NewEncoder(w).Encode(resp)
	default:
		w.WriteHeader(http.StatusNotFound)
	}
}

// Handles ?key= parameter query for
func (h *CacheHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	q := r.URL.Query()
	key := q.Get("key")
	if key == "" {
		logrus.Error("Invalid request: empty key")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	data, err := h.Cache.Get(key)
	if err != nil {
		logrus.Error("No key found in cache: ", err)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	type Response struct {
		Data string `json:"data"`
	}
	json.NewEncoder(w).Encode(&Response{Data: data.Value})
}
