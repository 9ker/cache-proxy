// +build !integration

package cache_test

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/9ker/cache-proxy/cache"
	"gitlab.com/9ker/cache-proxy/config"
)

var (
	alwaysHit cache.Service
	cacheGet  func(key string) (*cache.Data, error)
)

type mockExternalCache struct {
}

func (m *mockExternalCache) Get(key string) (*cache.Data, error) {
	return cacheGet(key)
}

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestNewCache(t *testing.T) {
	alwaysHit = &mockExternalCache{}
	cacheGet = func(key string) (*cache.Data, error) {
		return &cache.Data{}, nil
	}
	cache := cache.NewCache(&config.Config{}, alwaysHit)
	require.NotNil(t, cache)
}

func TestCacheGet(t *testing.T) {
	alwaysHit = &mockExternalCache{}
	c := cache.NewCache(&config.Config{CacheSize: 1, CacheTTL: 10}, alwaysHit)
	require.NotNil(t, c)
	c.Start()
	fixtures := []struct {
		data *cache.Data
	}{
		{data: &cache.Data{Key: "key1", Value: "value1"}},
		{data: &cache.Data{Key: "key2", Value: "value2"}},
	}
	for _, fxn := range fixtures {
		cacheGet = func(key string) (*cache.Data, error) {
			return fxn.data, nil
		}
		result, err := c.Get(fxn.data.Key)
		require.NoError(t, err)
		require.Equal(t, fxn.data, result)
	}
}

func TestCacheSweep(t *testing.T) {
	alwaysHit = &mockExternalCache{}
	called := 0
	c := cache.NewCache(&config.Config{CacheSize: 3, CacheTTL: 1}, alwaysHit)
	require.NotNil(t, c)
	c.Start()
	fixtures := []struct {
		data *cache.Data
	}{
		// Miss local
		{data: &cache.Data{Key: "key1", Value: "alwaysHit"}},
		// Miss local
		{data: &cache.Data{Key: "key1", Value: "alwaysHit"}},
	}
	for _, fxn := range fixtures {
		cacheGet = func(key string) (*cache.Data, error) {
			called++
			return fxn.data, nil
		}
		result, err := c.Get(fxn.data.Key)
		require.NoError(t, err)
		require.Equal(t, fxn.data, result)
		time.Sleep(time.Second * 2)
	}
	require.Equal(t, 2, called)
}

func TestCacheOOM(t *testing.T) {
	alwaysHit = &mockExternalCache{}
	called := 0
	c := cache.NewCache(&config.Config{CacheSize: 1, CacheTTL: 1}, alwaysHit)
	require.NotNil(t, c)
	c.Start()
	fixtures := []struct {
		data *cache.Data
	}{
		// Miss on local
		{data: &cache.Data{Key: "key1", Value: "alwaysHit"}},
		// Miss on local
		{data: &cache.Data{Key: "key2", Value: "alwaysHit"}},
		// Miss on local
		{data: &cache.Data{Key: "key1", Value: "alwaysHit"}},
		// Hit on local
		{data: &cache.Data{Key: "key1", Value: "alwaysHit"}},
	}
	for _, fxn := range fixtures {
		cacheGet = func(key string) (*cache.Data, error) {
			called++
			return fxn.data, nil
		}
		result, err := c.Get(fxn.data.Key)
		require.NoError(t, err)
		require.Equal(t, fxn.data, result)
	}
	require.Equal(t, 3, called)
}
