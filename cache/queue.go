package cache

import (
	"fmt"
	"sync"
)

type queue struct {
	keys []string
	lock sync.RWMutex
}

func (q *queue) add(key string) {
	q.lock.Lock()
	defer q.lock.Unlock()
	q.keys = append(q.keys, key)
}

func (q *queue) pop() error {
	if !q.empty() {
		q.lock.Lock()
		defer q.lock.Unlock()
		q.keys = q.keys[1:]
		return nil
	}
	return fmt.Errorf("pop error: empty queue")
}

func (q *queue) head() (string, error) {
	if !q.empty() {
		q.lock.Lock()
		defer q.lock.Unlock()
		return q.keys[0], nil
	}
	return "", fmt.Errorf("head error: empty queue")
}

func (q *queue) moveToTail(key string) error {
	if !q.empty() {
		q.lock.Lock()
		defer q.lock.Unlock()
		for idx, k := range q.keys {
			if k == key {
				q.keys = append(q.keys[:idx], q.keys[idx+1:]...)
				q.keys = append(q.keys, key)
				return nil
			}
		}
	}
	return fmt.Errorf("remove error: invalid key %s ", key)
}

func (q *queue) size() int {
	return len(q.keys)
}

func (q *queue) empty() bool {
	return len(q.keys) == 0
}
