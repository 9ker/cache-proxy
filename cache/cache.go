package cache

import (
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/9ker/cache-proxy/config"
)

// Cache struct
type Cache struct {
	TTL     int64
	Cap     int
	Queue   queue
	Hash    map[string]*Entry
	Backing Service
	Done    chan bool
	// handling concurrent requests
	requests chan request
}

type request struct {
	key      string
	response chan *Data
}

// Entry of cache
type Entry struct {
	Ts    time.Time
	Data  *Data
	Ready chan bool
}

// Data set
type Data struct {
	Key   string
	Value string `json:"value"`
}

// Service interface
type Service interface {
	Get(key string) (*Data, error)
}

// NewEntry creates empty entry
func NewEntry() *Entry {
	return &Entry{
		Ts:    time.Now(),
		Ready: make(chan bool),
	}
}

// NewCache initialize cache instance
func NewCache(c *config.Config, bc Service) *Cache {
	return &Cache{
		requests: make(chan request, c.MaxConn),
		TTL:      c.CacheTTL,
		Cap:      c.CacheSize,
		Backing:  bc,
		Hash:     make(map[string]*Entry, c.CacheSize),
		Done:     make(chan bool),
	}
}

// Get retrive data from cache
func (c *Cache) Get(key string) (*Data, error) {
	response := make(chan *Data)
	c.requests <- request{key, response}
	result := <-response
	if result == nil {
		return nil, fmt.Errorf("Key %s not found in cache", key)
	}
	return result, nil
}

// Set adds new entry into cache
func (c *Cache) Set(e *Entry, key string, response chan<- *Data) {
	<-e.Ready
	// Cap is 0
	if c.Cap == 0 || e.Data == nil {
		response <- e.Data
		return
	}

	// Queue is full, pop from head and remove from hash
	if c.Queue.size() == c.Cap {
		removed, err := c.Queue.head()
		if err != nil {
			logrus.Errorf("Set: %s", err)
		}
		delete(c.Hash, removed)
		logrus.Infof("Set: %s out of space", removed)
		c.Queue.pop()
	}
	// Key in Queue and Hash, rotate
	if !c.Queue.empty() {
		c.Queue.moveToTail(key)
		e.Ts = time.Now()
	}
	c.Queue.add(key)
	c.Hash[key] = e
	response <- e.Data
}

// Start cache
func (c *Cache) Start() {
	logrus.Info("Starting cache...")
	// Hanlding concurrent requests
	go func() {
		for {
			req := <-c.requests
			entry := c.Hash[req.key]
			if entry == nil {
				logrus.Info("MISS local cache")
				entry = NewEntry()
				go c.getFromBacking(entry, req.key)
			} else {
				logrus.Info("HIT local cache")
			}
			go c.Set(entry, req.key, req.response)
		}
	}()
	// Handling TTL cleanup
	go func() {
		logrus.Info("Starting cache clock")
		// sweep every seconds
		ticker := time.NewTicker(time.Second)
		defer ticker.Stop()
		for {
			select {
			case t := <-ticker.C:
				c.sweep(t)
			case <-c.Done:
				return
			}
		}
	}()
}

func (c *Cache) sweep(ts time.Time) {
	for !c.Queue.empty() {
		key, err := c.Queue.head()
		if err != nil {
			break
		}
		entry, ok := c.Hash[key]
		if !ok {
			break
		}
		if ts.Unix()-entry.Ts.Unix() < c.TTL {
			break
		}
		logrus.Infof("Sweep: %s out of live", key)
		delete(c.Hash, key)
		err = c.Queue.pop()
		if err != nil {
			logrus.Errorf("Sweep: %s", err)
		}
	}
}

func (c *Cache) getFromBacking(e *Entry, key string) {
	data, err := c.Backing.Get(key)
	if err != nil {
		logrus.Infof("MISS backing cache")
	} else {
		logrus.Infof("HIT backing cache")
	}
	e.Data = data
	close(e.Ready)
}
