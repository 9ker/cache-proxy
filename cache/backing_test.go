// +build !integration

package cache_test

import (
	"testing"

	"github.com/alicebob/miniredis"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/9ker/cache-proxy/cache"
	"gitlab.com/9ker/cache-proxy/config"
)

func TestConnect(t *testing.T) {
	c := &config.Config{RedisHost: "127.0.0.1", RedisPort: "6379"}
	bc := cache.NewBackingCache(c)

	_, err := bc.Connect()
	require.NoError(t, err)
}

func TestPing(t *testing.T) {
	mr, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	c := &config.Config{RedisHost: mr.Host(), RedisPort: mr.Port()}
	bc := cache.NewBackingCache(c)
	assert.True(t, bc.Ping())
}
