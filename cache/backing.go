package cache

import (
	"github.com/go-redis/redis/v7"
	"gitlab.com/9ker/cache-proxy/config"
)

// BackingCache struct
type BackingCache struct {
	Addr string
	Port string
}

// NewBackingCache initialize redis backend
func NewBackingCache(c *config.Config) *BackingCache {
	return &BackingCache{
		Addr: c.RedisHost,
		Port: c.RedisPort,
	}
}

// Connect using redis client
func (bc *BackingCache) Connect() (redis.Cmdable, error) {
	return redis.NewClient(&redis.Options{
		Addr: bc.Addr + ":" + bc.Port,
	}), nil
}

// Ping redis instance
func (bc *BackingCache) Ping() bool {
	c, err := bc.Connect()
	if err != nil {
		return false
	}
	return c.Ping().Err() == nil
}

// Get from redis
func (bc *BackingCache) Get(key string) (*Data, error) {
	client, err := bc.Connect()
	if err != nil {
		return nil, err
	}
	val, err := client.Get(key).Result()
	if err != nil {
		return nil, err
	}

	return &Data{Key: key, Value: val}, nil
}
