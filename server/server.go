package server

import (
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/9ker/cache-proxy/cache"
	"gitlab.com/9ker/cache-proxy/config"
)

// Server struct
type Server struct {
	Config   *config.Config
	Cache    *cache.Cache
	Router   *mux.Router
	HTTP     *http.Server
	errChan  chan error
	quitChan chan os.Signal
}

// NewServer initialize a Server
func NewServer(c *config.Config, localC *cache.Cache) *Server {
	router := mux.NewRouter()
	// Logging middleware http request -> stdout
	// Use an existing logging handler with default
	handler := handlers.LoggingHandler(os.Stdout, router)
	return &Server{
		Config: c,
		Router: router,
		HTTP: &http.Server{
			Addr:         ":" + c.HTTPPort,
			Handler:      handler,
			ReadTimeout:  10 * time.Second,
			WriteTimeout: 10 * time.Second,
		},
		Cache:    localC,
		errChan:  make(chan error),
		quitChan: make(chan os.Signal, 1),
	}
}

// Run starts the server
func (s *Server) Run() {
	go func() {
		logrus.Infof("listening and serving HTTP %s", s.Config.HTTPPort)
		if err := s.HTTP.ListenAndServe(); err != nil {
			s.errChan <- err
		}
	}()

	signal.Notify(s.quitChan, syscall.SIGINT, syscall.SIGTERM)

	select {
	case err := <-s.errChan:
		logrus.Error("HTTP server error:", err)
		return
	case sig := <-s.quitChan:
		logrus.Info(sig.String(), "caught, shutting down")
		return
	}
}
