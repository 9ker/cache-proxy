// +build !integration

package server_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/9ker/cache-proxy/cache"
	"gitlab.com/9ker/cache-proxy/config"
	"gitlab.com/9ker/cache-proxy/server"
)

func TestServerCreate(t *testing.T) {
	config := &config.Config{}
	cache := &cache.Cache{}
	server := server.NewServer(config, cache)
	require.NotNil(t, server)
}
