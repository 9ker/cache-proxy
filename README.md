# cache-proxy

A transparent HTTP proxy service for Redis


<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#about">About The Project</a></li>
    <li><a href="#specs">Spec</a></li>
    <li><a href="#complexity">Spec</a></li>
    <li><a href="#prerequisites">Prerequisites</a></li>
    <li><a href="#quick-start">Quick Start</a></li>
    <li>
        <a href="#usage">Usage</a>
        <ul>
            <li><a href="#build">Build & Run</a></li>
            <li><a href="#config">Configuration</a></li>
            <li><a href="#testing">Testing</a></li>
            <li><a href="#bootstrap-dataset">Bootstrapping</a></li>
      </ul>
    </li>
    <li><a href="#troubleshooting">Troubleshooting</a></li>
    <li><a href="#requirements">Requirements</a></li>
    <li><a href="#improvements">Future Improvment</a></li>
  </ol>
</details>

## About

Cache-proxy is a HTTP server that provides 2 endpoint accepting incoming traffic.

Cache-proxy provides a cache internall with configuration `size` and `ttl`.

Cache-proxy supports concurrent request and parallel processing with configured `max.conn` limits.

Currently it suppors simple `k/v` pair type with [proper url query parameter](https://en.wikipedia.org/wiki/Query_string) as `key`.

The internal cache will evict keys under two conditions:

* `key` out of `TTL` window
* least recently used `key` when out of space



## Specs

* `/` **endpoint**

  Accetps all http request methods `GET`, `POST`, `DELTE`, etc...

  Returns a generic message can be used as health-check
  ```json
  {"version":"0.0.1","name":"Proxy Server for Redis"}
  ```
* `/cache` **endpoint**

  Accepts only `GET` request with query paramet `?key` for retrieving value from cache

  Returns `200` and data
  ```json
  {"data":"{value}"}
  ```

  Returns `404` if key not found

## Complexity

Internal cache uses both Queue and HashMap structure to holds the key/value pair
Both SET and GET operation takes O(1) time and space complexity depends on the configuration
**cache.size**


## Prerequisites

* Docker `19.03.13+`
* docker-compose `1.27.4+`
* go `1.14+`

*Default build environemt is in MacOS
If you're running in Linux environemnt, should use Docker only command*

## Quick Start


1. Clone repo
   ```sh
   git clone https://gitlab.com/9ker/cache-proxy
   ```
2. Start server and redis with docker-compose (with pre-build config `config.yaml`)
   ```sh
   make start-all
   ```
3. Curl localhost:8080
   ```
   curl -v localhost:8080
   ```
4. Fetch a key from server
   ```
   curl -v localhost:8080/cache?key=anchor
   ```

*NOTE: sample `config.yaml` is committed for the sake of this assignment*


## Usage

### Build

* Build server binary
  ```
  make build-linux
  ./cache-proxy -c build/config.yaml
  ```

* Build server docker image
  ```
  make container
  ```

* Build & Run server in MacOS (*require MacOS**)
  ```
  make run-server-local
  ```

### Config

* Config required

  Take a look at `config.yaml` as default for **docker-compose** run
  ```yaml
  env: <environment> i.e. production/staging
  http.port: <server port> i.e. 8080
  cache.size: <cache capacity> i.e. 10000
  cache.ttl: <cache ttl(s)> i.e. 600
  max.conn: <maximum concurrent connections> i.e. 60
  redis.host: <redis host>
  redis.port: <redis port>
  ```


### Testing

* Run unit test
   ```sh
   make test
   ```
* Run integration test
   ```sh
   make run-integration
   ```

### Bootstrap Dataset

local redis start with running command in `bin/bootstrap.redis`

* Edit key/value pair with `SET` command
  ```sh
  vim bin/bootstrap.redis
  ```

* Restart redis instance
  ```sh
  docker-compose restart redis
  ```


## Troubleshooting

### Common Issues

No redis connection
  ```sh
  ERRO Server Init: Redis not available
  ```
Make sure redis is running and server can talk to it

Check `config.yaml` for configuration


No proper config
  ```sh
  panic: Error reading config file from path: open config.yaml: no such file or directory
  ```
Run with `-c` flag to set config path

## Requirements


- [x] **HTTP web service**
- [x] **Single backing instance**
- [x] **Cached GET**
- [x] **Eviction**
- [x] **Fixed key size**
- [x] **Sequential concurrent processing** (`max.conn == 1`)
- [x] **Configuration**
- [x] **System tests**
- [x] **Platform**
- [x] **Minimal commands for build & test**
- [x] **Documentation**
- [x] **Parallel concurrent processing**


## Improvements

* Update API so it takes more complex data type
* Make data model it's own moduel so it's extendable for future usage
* Add benchmark test
* Don't comit config file into git and use ConfigMap or Vault to store config
* Hook up with GitLab CI/CD pipeline for production use