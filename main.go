package main

import (
	"flag"

	"github.com/sirupsen/logrus"
	"gitlab.com/9ker/cache-proxy/cache"
	"gitlab.com/9ker/cache-proxy/config"
	"gitlab.com/9ker/cache-proxy/route"
	"gitlab.com/9ker/cache-proxy/server"
)

var (
	cfgPath string
	debug   bool
)

func main() {
	logger := logrus.New()
	flag.StringVar(&cfgPath, "c", "config.yaml", "Path to config file")
	flag.BoolVar(&debug, "d", false, "Run in debug mode")
	flag.Parse()

	conf, err := config.ParseConfig(cfgPath)
	if err != nil {
		panic(err)
	}
	if debug {
		logger.SetLevel(logrus.DebugLevel)
	}
	// Get redis
	redis := cache.NewBackingCache(conf)
	if !redis.Ping() {
		logrus.Error("Server Init: Redis not available")
	}

	localC := cache.NewCache(conf, redis)
	// Staring cache
	go localC.Start()
	s := server.NewServer(conf, localC)
	s.Router.Handle("/", &route.RootHandler{})
	s.Router.Handle("/cache", &route.CacheHandler{s.Cache})
	s.Run()
}
