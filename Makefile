BUILD_ID ?= $(shell git rev-parse --short HEAD 2> /dev/null || echo empty-commit)
IMG_NAME := $(notdir $(shell pwd))

.PHONY: help build container test run-server start-all fmt run-redis build-integration run-integration
help: ## List targets and descriptions
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build-linux: ## Build the main Go server (linux/amd64)
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -v -o build/$(IMG_NAME) .

container: build-linux ## Build deployable container with config
	docker build -f build/Dockerfile -t $(IMG_NAME):$(BUILD_ID) .
	docker tag $(IMG_NAME):$(BUILD_ID) $(IMG_NAME):latest

test: ## Run unit test
	go test -v -tags=unit ./...

run-server-local: ## Run cache-proxy locally
	CGO_ENABLED=0 go build -o $(IMG_NAME) .
	./$(IMG_NAME) -c build/config.yaml

start-all: build-linux ## Start all services in docker compose
	docker-compose -f build/docker-compose.yml up -d

stop-all: ## Stop all services in docker compose
	docker-compose -f build/docker-compose.yml kill

fmt: ## Run go fmt on all files
	go fmt -x ./...

run-redis: ## Run a local redis container export port 6379
	docker run --name local-redis -p 6379:6379 -d redis:6.0

redis-sh: ## Use for redis-cli into local redis container
	docker exec -it local-redis bash

build-integration: ## Build integration docker images
	cd integration && \
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go test -v -c -tags=integration -o integration.test && \
	docker-compose -f docker-compose.itgtest.yml build

run-integration: build-integration ## Run integration in docker with docker compose
	docker-compose -f integration/docker-compose.itgtest.yml up --force-recreate --abort-on-container-exit --exit-code-from integration-test
