// +build !integration

package config_test

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/9ker/cache-proxy/config"
)

func TestParseConfigFile(t *testing.T) {
	fixtures := map[config.Config]string{
		{Env: ""}:        `env: ""`,
		{Env: "testing"}: `env: "testing"`,
		{Env: "testing", HTTPPort: "8080"}: `
env: testing
http.port: 8080`,
		{Env: "testing", HTTPPort: "8080", RedisHost: "localhost", RedisPort: "6379"}: `
env: testing
http.port: 8080
redis.host: localhost
redis.port: 6379`,
		{Env: "testing", HTTPPort: "8080", RedisHost: "localhost", MaxConn: 10, RedisPort: "6379", CacheSize: 512, CacheTTL: 60}: `
env: testing
http.port: 8080
cache.size: 512
max.conn: 10
cache.ttl: 60
redis.host: localhost
redis.port: 6379`,
	}

	for cfg, fixture := range fixtures {
		func() {
			// Setup
			f, err := ioutil.TempFile("", "test-config")
			defer os.Remove(f.Name())
			require.NoError(t, err)

			_, err = f.WriteString(fixture)
			require.NoError(t, err)
			err = f.Close()
			require.NoError(t, err)

			// Verification
			conf, err := config.ParseConfig(f.Name())
			require.NoError(t, err)

			require.Equal(t, cfg.Env, conf.Env)
			require.Equal(t, cfg.HTTPPort, conf.HTTPPort)
			require.Equal(t, cfg.CacheSize, conf.CacheSize)
			require.Equal(t, cfg.CacheTTL, conf.CacheTTL)
			require.Equal(t, cfg.RedisHost, conf.RedisHost)
			require.Equal(t, cfg.RedisPort, conf.RedisPort)
		}()
	}
}
