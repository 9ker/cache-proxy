package config

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Config for the cache server
type Config struct {
	Env      string `yaml:"env"`
	HTTPPort string `yaml:"http.port"`
	// Size represents as # of keys
	CacheSize int `yaml:"cache.size"`
	// TTL default seconds
	CacheTTL int64 `yaml:"cache.ttl"`
	// Max concurrent process
	MaxConn   int    `yaml:"max.conn"`
	RedisHost string `yaml:"redis.host"`
	RedisPort string `yaml:"redis.port"`
}

// ParseConfig loads a config based on path provided
func ParseConfig(path string) (*Config, error) {
	buf, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("Error reading config file from path: %s", err)
	}

	c := &Config{}
	err = yaml.Unmarshal(buf, c)
	if err != nil {
		return nil, fmt.Errorf("Error parsing config file: %s", err)
	}

	return c, nil
}
